# -*- coding: utf-8 -*-
"""
Created on Tue Dec 26 15:10:08 2017

@author: enovi
"""
##License: GPL 3.0

##Bike Race
import random
game_loop      = True
player_cards   = []
computer_cards = []
miles          = 0
comp_miles     = 0

def main():
    choice = 'y'
    while choice == 'y':
        print("Welcome to Bike Race!")
        set_defaults()
        bike_race()
        choice = input('New game y/n? ')
        if choice == 'y':
            print ('Starting new game')
            choice = 'y'
        if choice == 'Y':
            print ('Starting new game')
            choice = 'y'

def set_defaults():
    global game_loop
    global player_cards
    global computer_cards
    global miles
    global comp_miles
    game_loop      = True
    player_cards   = []
    computer_cards = []
    miles          = 0
    comp_miles     = 0
    
def bike_race():
    global game_loop
    global miles
    global comp_miles
    while game_loop == True:
        draw_cards()
        play_cards()
        if miles >= 100:
            print('You won the race!')
            game_loop = False
        if comp_miles >= 100:
            print('You lost the race!')
            game_loop = False
        
def draw_cards():
    global player_cards
    global computer_cards
    while len(player_cards) < 4: 
        player_cards.append(random.randint(1,5))
    while len(computer_cards) < 4:    
        computer_cards.append(random.randint(1,5))     

def play_cards():
    global player_cards
    global computer_cards
    global miles
    global comp_miles
    choice = ''
    comp_choice = ''
    print('Your cards are:',player_cards)
    print('You have traveled {} miles'.format(miles))
    print('Computer cards are:',computer_cards)
    print('Computer has traveled {} miles'.format(comp_miles))
    choice = input("Select card 1, 2, 3, or 4: ")
    ## Player round
    if choice == '1':
        miles += player_cards[0] * 5
        del player_cards[0]
    elif choice == '2':    
        miles += player_cards[1] * 5
        del player_cards[1]
    elif choice == '3':    
        miles += player_cards[2] * 5
        del player_cards[2]
    elif choice == '4':    
        miles += player_cards[3] * 5
        del player_cards[3]
    else:
        print('Invalid choice')
    #Computer round
    comp_choice = str(random.randint(1,4))
    if comp_choice == '1':
        comp_miles += computer_cards[0] * 5
        del computer_cards[0]
    elif comp_choice == '2':    
        comp_miles += computer_cards[1] * 5
        del computer_cards[1]
    elif comp_choice == '3':    
        comp_miles += computer_cards[2] * 5
        del computer_cards[2]
    elif comp_choice == '4':    
        comp_miles += computer_cards[3] * 5
        del computer_cards[3]
        
main()        
        
        
        
        
        
        
        